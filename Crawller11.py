import requests
import csv
from lxml import html
from pymongo import MongoClient

                                        #Cotação Soja

connection = MongoClient("mongodb+srv://diegoantunes32:dacosta32@cluster0-ifc5n.mongodb.net/test?retryWrites=true&w=majority")
mydb = connection['cotacoes']

response = requests.get ("https://www.noticiasagricolas.com.br/cotacoes/soja")
site = html.fromstring(response.content)

print('-----CHICAGO (CME)-----')
print ('Plantio|   Data    |    Preço |   VAR ')
valorum =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[1]/td[2]/div')[0].text)

colunaum =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[1]/td[1]')[0].text)

varum = (site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[1]/td[3]')[0].text)

print("  Soja | {}  | R$ {}|  {} ".format(colunaum,valorum,varum))
mydb.soja.insert({
    'valor': valorum,
    'mes': colunaum,
    'var': varum
})

valordois =(site.xpath('///*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[2]/td[2]/div')[0].text)

colunadois =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[2]/td[1]')[0].text)

vardois =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[2]/td[3]')[0].text)

print("  Soja | {}  | R$ {}|  {} ".format(colunadois,valordois,vardois))
mydb.soja.insert({
    'valor': valordois,
    'mes': colunadois,
    'var': vardois
})
valortres =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[3]/td[2]/div')[0].text)

colunatres =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[3]/td[1]')[0].text)

vartres =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[3]/td[3]')[0].text)

print("  Soja | {}  | R$ {}|  {}".format(colunatres,valortres, vartres))
mydb.soja.insert({
    'valor': valortres,
    'mes': colunatres,
    'var': vartres
})
valorquatro =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[4]/td[2]/div')[0].text)

colunaquatro =(site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[4]/td[1]')[0].text)

varquatro= (site.xpath('//*[@id="sidebar"]/div[2]/div/div/div/table/tbody/tr[4]/td[3]')[0].text)

print("  Soja | {}  | R$ {}|  {}".format(colunaquatro,valorquatro, varquatro))


mydb.soja.insert({
    'valor': valorquatro,
    'mes': colunaquatro,
    'var': varquatro
})

print("-----BRASIL (B3)-----")
print ('Plantio|   Data    |    Preço  |   VAR ')
mvalorquatro =(site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[2]/td[2]/div')[0].text)

mcolunaquatro =(site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[2]/td[1]')[0].text)

mvarquatro= (site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[2]/td[3]')[0].text)

print(" {}    |  R$  {}|   {}".format(mcolunaquatro,mvalorquatro, mvarquatro))

cvalorquatro =(site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[3]/td[2]/div')[0].text)

ccolunaquatro =(site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[3]/td[1]')[0].text)

cvarquatro= (site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[3]/td[3]')[0].text)

print(" {}    |  R$ {}|   {}".format(ccolunaquatro,cvalorquatro, cvarquatro))

svalorquatro =(site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[1]/td[2]/div')[0].text)

scolunaquatro =(site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[1]/td[1]')[0].text)

svarquatro= (site.xpath('//*[@id="sidebar"]/div[7]/div/div/div/table/tbody/tr[1]/td[3]')[0].text)

print(" {}     |  R$ {} |  {}".format(scolunaquatro,svalorquatro, svarquatro))

print("-----NEW YORK (NYBOT)-----")
print ('Plantio|   Data    |    Preço  |   VAR ')
nycvalorquatro =(site.xpath('//*[@id="sidebar"]/div[8]/div/div/div/table/tbody/tr[1]/td[2]/div')[0].text)

nyccolunaquatro =(site.xpath('//*[@id="sidebar"]/div[8]/div/div/div/table/tbody/tr[1]/td[1]')[0].text)

nycvarquatro= (site.xpath('//*[@id="sidebar"]/div[8]/div/div/div/table/tbody/tr[1]/td[3]')[0].text)

print("{}|  R$ {} |  {}".format(nyccolunaquatro,nycvalorquatro, nycvarquatro))

tempminimasp= (site.xpath('//*[@id="site-header"]/div/div[2]/div[1]/div[1]/div[3]')[0].text)
tempmaxsp = (site.xpath('//*[@id="site-header"]/div/div[2]/div[1]/div[1]/div[2]')[0].text)

print ('___________________________')
print('Temperatura  Minima : {}'.format(tempminimasp))
print('Temperatura  Máxima : {}'.format(tempmaxsp))



                             
a = ('Data')
pr = ('Preco')
vr = ('VAR')
merc = open ('PIcrawler.csv', 'w')

try:
    
    writer = csv.writer(merc)
    writer.writerow((a,pr,vr))
    writer.writerow((colunaum,valorum,varum))
    writer.writerow((colunadois,valordois,vardois))
    writer.writerow((colunatres,valortres,vartres))
    writer.writerow((colunaquatro,valorquatro,varquatro))
finally:
    merc.close()